const express = require('express');
const bodyParser = require('body-parser');
const helmet = require('helmet');

const app = express();
const port = process.env.port || 3000;

app.use(helmet());
app.use(bodyParser.json());

app.use('/', require('./api'));

app.listen(port, () => {
    console.log(`Edspress API running in ${port}`);
});
