const router = require('express').Router();

router.get('/', (req, res) => {
    res.send("<h1>API quoeh</h1>");
})

router.use('/auth', require('./auth'));

module.exports = router;